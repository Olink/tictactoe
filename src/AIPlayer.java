public class AIPlayer extends IPlayer {
	public AIPlayer(Board board) {
		super(board);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void MakeMove() {
		MoveType[][] board = _board.GetBoard();
		
		if (board[1][1] == MoveType.None)
		{
			_board.MakeMove(1, 1, MoveType.O);
		} else {
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++)
				{
					if (_board.MakeMove(i,  j,  MoveType.O))
						return;
				}
			}
		}
	}
}
