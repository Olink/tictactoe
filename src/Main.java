public class Main {
    public static void main(String[] args) {
        Board board = new Board();
        IPlayer[] players = new IPlayer[2];
        
        players[0] = new HumanPlayer(board);
        players[1] = new AIPlayer(board);
    	int turn = 0;
        while(!board.IsWinner()){
        	System.out.println(board.toString());
        	players[turn % 2].MakeMove();
        	turn++;
        }
        System.out.println(board.toString());
        System.out.println("The game is over!!!!!!");
    }
}