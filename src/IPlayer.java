public abstract class IPlayer {
    protected Board _board;
	
	private IPlayer()
    {
    	
    }
	
	public IPlayer(Board board)
    {
		_board = board;
    }

    public abstract void MakeMove();
}
