public class Board {
    private final int BOARD_SIZE = 3;

    private MoveType _board[][];

    public boolean Finished = false;

    public Board()
    {
        _board = new MoveType[BOARD_SIZE][BOARD_SIZE];
        for(int i = 0; i < BOARD_SIZE; i++)
        {
            for(int j = 0; j < BOARD_SIZE; j++)
            {
                _board[i][j] = MoveType.None;
            }
        }
    }

    public MoveType[][] GetBoard()
    {
        return _board;
    }

    public boolean MakeMove(int x, int y, MoveType move)
    {
        if (_board[x][y] != MoveType.None)
            return false;

        _board[x][y] = move;
        return true;
    }

    public boolean IsWinner()
    {
        boolean winner = false;
        for(int i = 0; (i < BOARD_SIZE) && !winner; i++)
        {
            winner |= CheckRow(i);
            winner |= CheckColumn(i);
        }

        if(!winner)
        {
            MoveType potential = _board[0][0];
            if (potential != MoveType.None)
            {
                winner = (_board[1][1] == potential) && (_board[2][2] == potential);
            }
        }

        if(!winner)
        {
            MoveType potential = _board[0][2];
            if (potential != MoveType.None)
            {
                winner = (_board[1][1] == potential) && (_board[2][0] == potential);
            }
        }

        return winner;
    }

    private boolean CheckRow(int x)
    {
        MoveType potential = _board[x][0];
        if (potential != MoveType.None)
        {
            if ((_board[x][1] == potential) && (_board[x][2] == potential))
                return true;
        }
        return false;
    }

    private boolean CheckColumn(int y)
    {
        MoveType potential = _board[0][y];
        if (potential != MoveType.None)
        {
            if ((_board[1][y] == potential) && (_board[2][y] == potential))
                return true;
        }
        return false;
    }

    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        for(int i = 0; i < BOARD_SIZE; i++)
        {
            for(int j = 0; j < BOARD_SIZE; j++)
            {
                builder.append("|");
                switch(_board[i][j]) {
                    case None:
                        builder.append(" ");
                        break;
                    case O:
                        builder.append("O");
                        break;
                    case X:
                        builder.append("X");
                        break;
                }
            }
            builder.append("|\n");
        }

        return builder.toString();
    }
}
