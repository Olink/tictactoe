import java.util.InputMismatchException;
import java.util.Scanner;

public class HumanPlayer extends IPlayer {
    public HumanPlayer(Board board) {
		super(board);
		// TODO Auto-generated constructor stub
	}

	public void MakeMove()
    {
        Scanner reader = new Scanner(System.in);
        int index = -1;
        boolean madeMove = false;
        while (!madeMove) {
            while ((index < 0 || index > 8)) {
                reader = new Scanner(System.in);
                System.out.print("Please input the square you wish to move to {0-8}: ");
                try {
                    index = reader.nextInt();
                } catch (InputMismatchException e) {
            		index = -1;
                }
            }
            int x = index / 3;
            int y = index % 3;
            madeMove = _board.MakeMove(x, y, MoveType.X);
        }
    }
}
